import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

final String keyNotificationsEnabled = 'keyNotificationsEnabled';

void hideKeyboard(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
}

Future<bool> isPushNotificationsEnabled() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool(keyNotificationsEnabled) ?? true;
}

Future<void> setPushNotificationsEnabled(bool enabled) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(keyNotificationsEnabled, enabled);
}
