import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:promome/firebase/FirebaseHelper.dart';
import 'package:promome/navigation/BottomNavigationPage.dart';

import 'AppHelper.dart';

const appColor = Color.fromARGB(255, 15, 190, 124);
const primaryColor = Colors.green;
const cursorColor = Colors.white;

const firebaseTopic = 'oxko';

class PromoMeApp extends StatefulWidget {
  FirebaseMessaging firebaseMessaging;

  void subscribeToPushNotifications() {
    firebaseMessaging.subscribeToTopic(firebaseTopic);
  }

  void unsubscribeFromPushNotifications() {
    firebaseMessaging.unsubscribeFromTopic(firebaseTopic);
  }

  PromoMeApp() {
    FlutterCrashlytics().initialize();
    /*
    firebaseMessaging = FirebaseMessaging();
    firebaseMessaging.requestNotificationPermissions();
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onBackgroundMessage: promomeBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    isPushNotificationsEnabled().then((enabled) {
      subscribeToPushNotifications();
    });
     */
  }

  @override
  PromoMeAppState createState() => PromoMeAppState();

  static PromoMeApp instance;
  static PromoMeApp getInstance() {
    if (instance == null) {
      instance = PromoMeApp();
    }
    return instance;
  }
}

class PromoMeAppState extends State<PromoMeApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          cursorColor: cursorColor,
          cupertinoOverrideTheme: CupertinoThemeData(
            primaryColor: cursorColor,
          ),
          primaryColor: primaryColor,
          primarySwatch: primaryColor,
          buttonColor: primaryColor,
          accentColor: primaryColor,
          appBarTheme: AppBarTheme(
            color: appColor,
          )),
      home: BottomNavigationPage(),
    );
  }
}
