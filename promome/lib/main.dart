import 'package:flutter/material.dart';
import 'package:promome/app/PromoMeApp.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(PromoMeApp());
}
